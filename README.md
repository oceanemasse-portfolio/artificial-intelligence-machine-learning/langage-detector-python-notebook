_Par Masse Océane & Daudré--Treuil Prunelle_

**Un projet de NLP dans le cadre de l'UE Sciences des Données 2 de la Licence MIASHS**

Dans le présent projet, nous cherchons à classifier du texte en fonction de leur langue en utilisant la fréquence d'utilisation des lettres et bigrammes dans les différentes langues. Le présent document présentera comment nous avons pu collecter nos données sur wikipedia en utilisant les librairies requests et BeautifulSoup, puis une partie d'exploration des données. Enfin nous testerons plusieurs modèles de classification tout en cherchant à déterminer leurs paramètres.
